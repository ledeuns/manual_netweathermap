# manual_netweathermap

Create a weather map with Graphviz and base tools

Generate image with ``m4 netwmap.dot.m4 | neato -Tpng > netwmap.png``

#### netwmap.dot.m4 :
![png](netwmap.png)

#### netwmap2.dot.m4 :
![png](netwmap2.png)
