define(c0,``#f0f0f0'')
define(c10,``#a0f0a0'')
define(c20,``#00e0dd'')
define(c30,``#0ec1ff'')
define(c40,``#5cacff'')
define(c50,``#9593f2'')
define(c60,``#bf76d1'')
define(c70,``#ca55a5'')
define(c80,``#ba1842'')
define(c90,``#a01010'')

digraph structs {
graph [width=400 height=400]
edge [arrowhead=normal dir=both];
node [shape=box]
"Router A" -> "Customer" [headlabel="50%" taillabel="30%" color="c30:c50;0.5" len=2 penwidth=3]
"Router A" -> "Internet" [headlabel="10%" taillabel="20%" color="c20:c10;0.5" len=2 penwidth=6]
"Router A" -> "Customer 2" [headlabel="80%" taillabel="60%" color="c60:c80;0.5" len=2 penwidth=2]
}

