define(c0,``#f0f0f0'')
define(c10,``#a0f0a0'')
define(c20,``#00e0dd'')
define(c30,``#0ec1ff'')
define(c40,``#5cacff'')
define(c50,``#9593f2'')
define(c60,``#bf76d1'')
define(c70,``#ca55a5'')
define(c80,``#ba1842'')
define(c90,``#a01010'')

digraph structs {
graph [width=400 height=400]
edge [arrowhead=normal headclip=false len=1.5];
node [shape=box]
p1 [shape=none label=""]
p2 [shape=none label=""]
p3 [shape=none label=""]

"Router A" -> p1 [label="0%" color="c0" penwidth=3]
"Customer" -> p1 [label="30%" color="c30" penwidth=3]
"Router A" -> p2 [label="20%" color="c20" penwidth=6]
"Internet" -> p2 [label="10%" color="c10" penwidth=6]
"Router A" -> p3 [label="60%" color="c60" penwidth=2]
"Customer 2" -> p3 [label="80%" color="c80" penwidth=2]
}
